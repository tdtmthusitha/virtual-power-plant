package com.thusitha.thusitha;

import com.thusitha.thusitha.model.Battery;
import com.thusitha.thusitha.repository.BatteryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private BatteryRepository repository;

    @BeforeEach
    void setUp() {
        repository.save(new Battery("name", 1, 100));
    }

    @Test
    public void getFromRage() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/getFromRage?from=1&to=100",
                String.class)).contains("name");
    }
}