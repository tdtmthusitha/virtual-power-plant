package com.thusitha.thusitha.services;

import com.thusitha.thusitha.model.Battery;
import com.thusitha.thusitha.model.BatteryDto;
import com.thusitha.thusitha.model.RangeResponse;
import com.thusitha.thusitha.repository.BatteryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@RequiredArgsConstructor
public class BatteryService {
    private final BatteryRepository repository;

    public ResponseEntity<Battery> save(List<BatteryDto> list) {
        list.forEach(batteryDto -> repository.save(new Battery(batteryDto.getName(), batteryDto.getPostCode(), batteryDto.getWattCapacity())));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    public ResponseEntity<RangeResponse> getFromRage(int from, int to) {
        List<Battery> between = repository.findAllByWattCapacityBetween(from, to);
        if (between.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        List<String> names = new ArrayList<>();
        AtomicInteger total = new AtomicInteger();
        between.forEach(battery -> {
            names.add(battery.getName());
            total.addAndGet(battery.getWattCapacity());
        });
        int average = total.getPlain() / between.size();
        Collections.sort(names);
        return new ResponseEntity<>(new RangeResponse(names, total.get(), average), HttpStatus.OK);
    }
}
