package com.thusitha.thusitha.repository;

import com.thusitha.thusitha.model.Battery;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BatteryRepository extends JpaRepository<Battery, Long> {
    List<Battery> findAllByWattCapacityBetween(int from, int to);
}
