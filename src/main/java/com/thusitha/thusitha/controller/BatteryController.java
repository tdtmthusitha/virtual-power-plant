package com.thusitha.thusitha.controller;

import com.thusitha.thusitha.model.Battery;
import com.thusitha.thusitha.model.BatteryDto;
import com.thusitha.thusitha.model.RangeResponse;
import com.thusitha.thusitha.services.BatteryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class BatteryController {

    private final BatteryService batteryService;

    @PostMapping("save")
    ResponseEntity<Battery> save(@RequestBody List<BatteryDto> battery) {
        return batteryService.save(battery);
    }

    @GetMapping("getFromRage")
    ResponseEntity<RangeResponse> getFromRage(@RequestParam int from, @RequestParam int to) {
        return batteryService.getFromRage(from, to);
    }

}
