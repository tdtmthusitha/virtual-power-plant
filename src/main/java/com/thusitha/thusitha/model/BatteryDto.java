package com.thusitha.thusitha.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class BatteryDto {
    private String name;
    private int postCode;
    private int wattCapacity;
}
