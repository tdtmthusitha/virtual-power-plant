package com.thusitha.thusitha.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RangeResponse {
    private List<String> names;
    private int total;
    private int average;
}
